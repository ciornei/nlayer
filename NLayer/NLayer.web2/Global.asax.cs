﻿using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NLayer.web2
{
  public class MvcApplication : System.Web.HttpApplication
  {
    protected void Application_Start()
    {
      AreaRegistration.RegisterAllAreas();
      RouteConfig.RegisterRoutes(RouteTable.Routes);

      var container = new Container();
      var webReqLifeCycle = new WebRequestLifestyle();
      Services.Infrastructure.SimpleInjectorInitializer.InitializeContainer(container, webReqLifeCycle);
      container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
      container.RegisterMvcIntegratedFilterProvider();
      container.Verify();
      DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

    }
  }
}
