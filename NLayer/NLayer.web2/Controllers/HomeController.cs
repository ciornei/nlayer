﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLayer.Services.Products;

namespace NLayer.web2.Controllers
{
  public class HomeController : Controller
  {
    private readonly IproductsService productService;

    public HomeController(IproductsService productService)
    {
      this.productService = productService;
    }

    // GET: Home
    public ActionResult Index()
    {
      var cat = productService.GetCategories();
      ViewBag.Categories = cat;


      return View();
    }
  }
}