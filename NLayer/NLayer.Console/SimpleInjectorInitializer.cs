﻿using SimpleInjector;

namespace NLayer.Console
{
  public static class SimpleInjectorInitializer
  {

    public static Container Injectorcontainer { get; set; }

    public static void Initialize()
    {
      var container = new Container();

      //singleton lifeCycle
      var singletonLifetime = Lifestyle.Singleton;
      Services.Infrastructure.SimpleInjectorInitializer.InitializeContainer(container, singletonLifetime);
      Injectorcontainer = container;
    }

  }
}
