﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayer.Services.Dto;
using NLayer.Services.Users;
using NLayer.Services.Orders;
using NLayer.Services.Models;


namespace NLayer.Console
{
  class ProofOfConcept
  {

    private readonly IUserService personService;
    private readonly IOrderService orderService;

    public ProofOfConcept()
    {
      SimpleInjectorInitializer.Initialize();
      var container = SimpleInjectorInitializer.Injectorcontainer;
      personService = container.GetInstance<IUserService>();
      orderService = container.GetInstance<IOrderService>();
    }

    public void ShowPeople()
    {
      var persons = personService.GetAll();

      foreach (var person in persons)
      {
        System.Console.WriteLine("{0} {1} {2} {3}", person.ID, person.Email, person.FirstName, person.LastName);
      }
      
    }

    public void ShowOrders()
    {
      var orders = orderService.ShowOrders(930);

      foreach (var item in orders)
      {
        System.Console.WriteLine(item.OrderProducts.Select(a=> a.ProductID));
      }
    }

    public void AddOrderProduct()
    {
      var ordprod = new OrderProduct(350, 180, 32);
      orderService.AddOderProduct(ordprod);
    }

    public void AddOrder()
    {
      orderService.AddOrder(new OrderDto { OrderDate = DateTime.Now, UserID = 930, ShipmentAddressID = 10, OrderProducts = null });
    }
  }
}
