﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NLayer2.web.Startup))]
namespace NLayer2.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
