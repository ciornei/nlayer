﻿using SimpleInjector;
using SimpleInjector.Integration.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace NLayer2.web
{
  public class MvcApplication : System.Web.HttpApplication
  {
    protected void Application_Start()
    {
      AreaRegistration.RegisterAllAreas();
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);
      var container = new Container();
      var webReqLifeCycle = new WebRequestLifestyle();
      NLayer.Services.Infrastructure.SimpleInjectorInitializer.InitializeContainer(container, webReqLifeCycle);
      container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
      container.RegisterMvcIntegratedFilterProvider();
      container.Verify();
      DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

    }
  }
}
