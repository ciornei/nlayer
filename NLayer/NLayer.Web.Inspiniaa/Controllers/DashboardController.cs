﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLayer.Services.Dto;
using NLayer.Services.Users;
using NLayer.web3.Helpers;

namespace NLayer.Web.Inspiniaa.Controllers
{
  [AdminAcces]
  public class DashboardController : Controller
  {
    private readonly IUserService userService;

    public DashboardController(IUserService userService)
    {
      this.userService = userService;
    }
    // GET: Dashbord
    public ActionResult Index()
    {
      return View();
    }

    public ActionResult Test()
    {
      var users = userService.GetAll();

      return View(users);
    }

    [HttpPost]
    public string LogOut()
    {
      System.Web.HttpContext.Current.Session.Remove("LoggedUserId");
      System.Web.HttpContext.Current.Session.Remove("LoggedUserName");
      System.Web.HttpContext.Current.Session.Remove("LoggedUserRole");

      return "1";
    }
  }
}
