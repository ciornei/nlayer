﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLayer.Services;
using NLayer.Services.Users;
using NLayer.Web.Inspiniaa.Models;
using NLayer.Services.Dto;
using Omu.ValueInjecter;
using NLayer.Web.Inspiniaa.Helpers;

namespace NLayer.Web.Inspiniaa.Controllers
{
  public class HomeController : Controller
  {
    // GET: Home 
    private readonly IUserService userService;

    public HomeController(IUserService userService)
    {
      this.userService = userService;
    }
    [LoginAcces]
    public ActionResult Login()
    {

      return View();

    }

    [HttpPost]
    public ActionResult RequestLogin(LoginModel user1)
    {
      if(!ModelState.IsValid)
      {
        return View(user1);
      }

      UserDto user = new UserDto();
      user.InjectFrom(user1);

      string result = userService.LoginUser(user);
      switch (result)
      {
        case "0": return View("Error");
        case "1":
          user = userService.GetUserByEmail(user.Email);
          System.Web.HttpContext.Current.Session["LoggedUserId"] = user.ID;
          System.Web.HttpContext.Current.Session["LoggedUserName"] = user.FirstName + " " + user.LastName;
          System.Web.HttpContext.Current.Session["LoggedUserRole"] = user.Role;
          return RedirectToAction("Index", "Dashboard");
        default: return View("Error");
      }
     
    }
  }
}