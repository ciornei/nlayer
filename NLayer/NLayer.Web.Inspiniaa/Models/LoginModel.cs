﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NLayer.Web.Inspiniaa.Models
{
  public class LoginModel
  {
    [Required(ErrorMessage = "Tre' mail")]
    [DataType(DataType.EmailAddress)]
    [RegularExpression("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$", ErrorMessage = "Must be a valid email")]
    public string Email { get; set; }

    [RegularExpression(@"^(((?=.*[a-z])(?=.*[A-Z])(?=.*[\d]))|((?=.*[a-z])(?=.*[A-Z])(?=.*[\W]))|((?=.*[a-z])(?=.*[\d])(?=.*[\W]))|((?=.*[A-Z])(?=.*[\d])(?=.*[\W]))).{8,30}$", ErrorMessage = "Password must be in 8-30 range and  contain 3 out of the 4 of these: \n• lowercase letter \n• uppercase letter \n• number \n• special character ! @ $ % ^ &*( ) + ? ")]
    [DataType(DataType.Password)]
    [Required(ErrorMessage = "Tre' parola")]
    public string Password { get; set; }

  }
}