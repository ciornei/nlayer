﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using NLayer.Web.Inspiniaa.App_Start;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using System.Reflection;
using SimpleInjector.Integration.Web.Mvc;

namespace NLayer.Web.Inspiniaa
{
  public class Global : System.Web.HttpApplication
  {
    protected void Application_Start()
    {

      AreaRegistration.RegisterAllAreas();
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);

      var container = new Container();
      var webReqLifeCycle = new WebRequestLifestyle();
      Services.Infrastructure.SimpleInjectorInitializer.InitializeContainer(container, webReqLifeCycle);
      container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
      container.RegisterMvcIntegratedFilterProvider();
      container.Verify();
      DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
    }

  }
}