﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NLayer.web.Startup))]
namespace NLayer.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
