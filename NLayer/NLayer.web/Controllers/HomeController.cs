﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLayer.Services.Products;
using NLayer.Services.Dto;
using NLayer.Services.Users;

namespace NLayer.web.Controllers
{
  public class HomeController : Controller
  {
    private readonly IUserService userService;

    public HomeController(IUserService userService)
    {
      this.userService = userService;
    }

    public ActionResult Index()
    {
      return View();
    }

    public ActionResult About()
    {
      ViewBag.Message = "Your application description page.";

      return View();
    }

    public ActionResult Contact()
    {
      ViewBag.Message = "Your contact page.";

      return View();
    }

    public ActionResult Register()
    {
      return View();
    }

    [HttpPost]
    public ActionResult Register(UserDto user)
    {

      var repeatpass = Request.Form["repeatPass"];
      if (!repeatpass.Equals(null))
      {
        if (!user.Password.Equals(repeatpass))
        {
          return View(user);
        }
      }
      if (ModelState.IsValid)
      {
        userService.AddUser(user);

        return Redirect("Index");
      }
      return View(user);

    }

    public ActionResult Login()
    {
      return View();
    }

    
  }
}