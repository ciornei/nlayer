﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLayer.Services.Orders;
using NLayer.Services.Products;
using NLayer.Services.Users;
using NLayer.Services.Dto;
using Newtonsoft.Json;
using NLayer.Services.Models;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;

namespace NLayer.web.Controllers
{
  public class OrdersController : Controller
  {
    // GET: Orders

    private readonly IOrderService orderService;
    private readonly IUserService userService;
    private readonly IproductsService productService;
    // GET: User
    public OrdersController(IOrderService orderService, IproductsService productService, IUserService userService)
    {
      this.orderService = orderService;
      this.productService = productService;
      this.userService = userService;
    }

    public ActionResult Index()
    {
      var users = userService.GetAll();

      var cat = productService.GetCategories();
      ViewBag.Categories = cat;
      ViewBag.Users = users;

      return View();
    }

    [HttpPost]
    public string SeeOrders(string userId)
    {
      List<OrderDto> list = new List<OrderDto>();
      list = orderService.ShowOrders(Convert.ToInt32(userId));

      return JsonConvert.SerializeObject(list);
    }

    [HttpPost]
    public string GetProducts(string orderId)
    {
      List<OrderProductsDto> produse = new List<OrderProductsDto>();
      produse = productService.GetProductsByOrder(Convert.ToInt32(orderId));

      return JsonConvert.SerializeObject(produse);
    }
    [HttpPost]
    public string GetProductsByCat(string catId, string orderID)
    {

      var prods = productService.GetProductsByCat(Convert.ToInt32(catId), Convert.ToInt32(orderID));
      return JsonConvert.SerializeObject(prods);

    }

    [HttpPost]
    public string GetProductsByCategory(string catId)
    {

      var prods = productService.GetProductsByCat2(Convert.ToInt32(catId));
      return JsonConvert.SerializeObject(prods);

    }

    [HttpPost]
    public string RemoveOrderProduct(string orderId, string productId)
    {
      orderService.RemoveOrderProduct(Convert.ToInt32(orderId), Convert.ToInt32(productId));

      return "1";
    }

    [HttpPost]
    public string AddOrderProduct(string orderId, string productId, string quantity)
    {


      var op = new OrderProduct(Convert.ToInt32(orderId), Convert.ToInt32(productId), Convert.ToInt32(quantity));
      orderService.AddOderProduct(op);

      return "1";
    }

    [HttpPost]
    public string addOrder(string shipp, string userID)
    {
      var order = new OrderDto() { UserID = Convert.ToInt32(userID), ShipmentAddressID = Convert.ToInt32(shipp), OrderDate = DateTime.Now, OrderProducts = new List<OrderProduct>() };
      orderService.AddOrder(order);

      return "1";
    }

    [HttpPost]
    public string CreateNewOrder(string userID, string prods)
    {
      var order = new OrderDto();
      order.UserID = Convert.ToInt32(userID);
      order.OrderDate = DateTime.Now;
      var obj3 = JsonConvert.DeserializeObject<List<OrderProduct>>(prods);
      order.OrderProducts = obj3;
      orderService.AddOrder(order);
      return "1";
    }
  }

}