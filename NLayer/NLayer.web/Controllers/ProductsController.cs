﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLayer.Services.Products;
using NLayer.Services.Dto;

namespace NLayer.web.Controllers
{
  public class ProductsController : Controller
  {
    private readonly IproductsService productService;

    public ProductsController(IproductsService productService)
    {
      this.productService = productService;
    }


    public ActionResult Index(string search)
    {
      string formValue;

      var cat = productService.GetCategories();
      ViewBag.Categories = cat;


      if (!string.IsNullOrEmpty(Request.Form["search"]))
      {
        

        formValue = Request.Form["search"];
        var x = productService.FindByName(formValue);
        return View(x);
      }
      else
      {
        var wantedItems = productService.GetAll();

        foreach (var item in wantedItems)
        {
          item.CategoryName = productService.GetCategoryById(item.CategoryID);
        }

          return View(wantedItems);
      }
      
    }

    public ActionResult Create()
    {
      var cat = productService.GetCategories();
      ViewBag.Categories = cat;


      return View();
    }

    [HttpPost]
    public ActionResult Create(ProductDto product)
    {
      if (ModelState.IsValid)
      {
        productService.AddProduct(product);
        return Redirect("Index");
      }
      return View(product);
      
    }

    public ActionResult Edit(int id)
    {
      var cat = productService.GetCategories();
      ViewBag.Categories = cat;

      var prod = productService.Get(id);
      return View(prod);
    }

    [HttpPost]
    public ActionResult Edit(ProductDto product)
    {
      if(ModelState.IsValid)
      {
        productService.UpdateProduct(product);
        return Redirect("Index");
      }
      return View(product);

    }

    [HttpPost]
    public string AddProduct(string name, string price)
    {
      var product = new ProductDto() {Name=name, Price= Convert.ToDecimal(price) };
      productService.AddProduct(product);

      return "1";
    }

    [HttpPost]
    public string EditProduct(int id, string name, string price, int cat)
    {
      var product = new ProductDto() {ID= Convert.ToInt32(id), Name = name, Price = Convert.ToDecimal(price) , CategoryID=cat};
      productService.UpdateProduct(product);

      return "1";
    }
    [HttpPost]
    public string DeleteProduct(int id)
    {
      productService.DeleteProduct(Convert.ToInt32(id));
      return "1";
    }

  }
}