﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLayer.Services.Users;
using NLayer.Services.Dto;

namespace NLayer.web.Controllers
{
  public class UserController : Controller
  {

    private readonly IUserService userService;
    // GET: User
    public UserController(IUserService userService)
    {
      this.userService = userService;
    }


    public ActionResult Index()
    {
      var users = userService.GetAll();

      return View(users);
    }

    public ActionResult Create()
    {
      return View();
    }

    [HttpPost]
    public ActionResult Create(UserDto user)
    {
      var repeatpass = Request.Form["repeatPass"];
      if(!user.Password.Equals(repeatpass))
      {
        return View(user);
      }
      if(ModelState.IsValid)
      {
        userService.AddUser(user);

        return Redirect("Index");
      }
      return View(user);

    }

    public ActionResult Edit(int id)
    {
      var user = userService.GetUserById(id);
      return View(user);

    }



    [HttpPost]
    public string CreateUser(string lastName, string firstName, string password, string email, string role)
    {
      var user = new UserDto() { FirstName = firstName, LastName = lastName, Password = password, Email = email, Role = role };
      userService.AddUser(user);
        
      return "1";
    }

    [HttpPost]
    public string EditUser( string id, string lastName, string firstName, string email, string role)
    {
      var user = new UserDto() { ID = Convert.ToInt32(id), FirstName = firstName, LastName = lastName,  Email = email, Role = role };
      userService.Update(user);
      return "1";
    }
    [HttpPost]
    public string DeleteUser(string userID)
    {
      userService.DeleteUser(Convert.ToInt32(userID));
      return "1";
    }
  }
}