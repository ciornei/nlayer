﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NLayer.web3.Startup))]
namespace NLayer.web3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
