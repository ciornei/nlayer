﻿using System.Web.Mvc;
using NLayer.Services.Products;
using NLayer.Services.Users;
using NLayer.Services.Dto;
using System.Threading.Tasks;
using NLayer.web3.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Web;
using Omu.ValueInjecter;
using System.Text;
using System.Security.Cryptography;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Linq;

namespace NLayer.web3.Controllers
{
  public class HomeController : Controller
  {
    private readonly IUserService userService;
    private readonly IproductsService productService;

    private ApplicationSignInManager _signInManager;
    private ApplicationUserManager _userManager;

    public ApplicationSignInManager SignInManager
    {
      get
      {
        return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
      }
      private set
      {
        _signInManager = value;
      }
    }

    public ApplicationUserManager UserManager
    {
      get
      {
        return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
      }
      private set
      {
        _userManager = value;
      }
    }

    public HomeController(IproductsService productService, IUserService userService)
    {
      this.productService = productService;
      this.userService = userService;

    }
    // GET: Home
    public ActionResult Index()
    {
      var cat = productService.GetAll2();
      
      ViewBag.Products = cat;

      return View();
    }

    public ActionResult Register()
    {
      return View();
    }

    [HttpPost]
    public ActionResult Register(UserDto user)
    {

      var repeatpass = Request.Form["repeatPass"];
      if (!repeatpass.Equals(""))
      {
        if (!user.Password.Equals(repeatpass))
        {
          return View(user);
        }
      }
      user.Role = "admin";
      ModelState.Remove("Role");
      if (ModelState.IsValid)
      {
        userService.AddUser(user);

        return Redirect("Index");
      }
      return View(user);

    }

    public ActionResult Login()
    {
      return View();
    }

    [HttpPost]
    public string Logout()
    {

      System.Web.HttpContext.Current.Session.Remove("LoggedUserId");
      System.Web.HttpContext.Current.Session.Remove("LoggedUserName");
      System.Web.HttpContext.Current.Session.Remove("LoggedUserRole");

      return "1";
    }


    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public  ActionResult Login(LoginViewModel model, string returnUrl)
    {
     
      if (!ModelState.IsValid)
      {
        return View(model);
      }

      UserDto user = new UserDto();
      user.InjectFrom(model);

      string result = userService.LoginUser(user);
      switch(result)
      {
        case "0": return View("Error");
        case "1":
          user = userService.GetUserByEmail(user.Email);
          System.Web.HttpContext.Current.Session["LoggedUserId"] = user.ID;
          System.Web.HttpContext.Current.Session["LoggedUserName"] = user.FirstName + " " +user.LastName;
          System.Web.HttpContext.Current.Session["LoggedUserRole"] = user.Role;
          return RedirectToAction("Index", "Home");
        default: return View("Error");
      }
     
    }

    public ActionResult Logged(LoginViewModel user)
    {
      return View(user);
    }

    private string CreateHash(string password)
    {
      StringBuilder Sb = new StringBuilder();
      using (SHA256 hash = SHA256Managed.Create())
      {
        Encoding enc = Encoding.UTF8;
        Byte[] result = hash.ComputeHash(enc.GetBytes(password));
        foreach (Byte b in result)
          Sb.Append(b.ToString("x2"));
      }
      return Sb.ToString().Substring(0, 50);
    }

    
    public string SessionCart(int id)
    {
      List<int> prods = new List<int>();
      if(System.Web.HttpContext.Current.Session["prods"] == null)
      {
        prods.Add(id);
        System.Web.HttpContext.Current.Session["prods"] = prods;
      }
      else
      {
        prods = (List<int>)System.Web.HttpContext.Current.Session["prods"];
        prods.Add(id);
        System.Web.HttpContext.Current.Session["prods"] = prods;

      }

      return "1";
    }

    public string GetCartLength()
    {
      
      if(System.Web.HttpContext.Current.Session["prods"] != null)
      {
        List<int> prods = new List<int>();
        prods = (List<int>)System.Web.HttpContext.Current.Session["prods"];
        return Convert.ToString(prods.Count);
      }

      return "0";

    }



    public string GetCurentCart()
    {
      List<int> prods = new List<int>();
      List<ProductDto> produse = new List<ProductDto>();
      if(System.Web.HttpContext.Current.Session["prods"] != null )
      {
        prods = (List<int>)System.Web.HttpContext.Current.Session["prods"];
        foreach(var item in prods)
        {
          produse.Add(productService.Get(item));
        }
        return JsonConvert.SerializeObject(produse);
      }
      
      return "0";

    }
    
    public ActionResult ForgotPassword()
    {
      return View();
    }
    
    public ActionResult ResetTemporaryCode(string code)
    {
      ViewBag.code = code;

      return View();
    }

    public string MailUs()
    {
      SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
      client.EnableSsl = true;
      client.Credentials = new System.Net.NetworkCredential("andrei.ciornei1@gmail.com", "PandaForce1");
      MailMessage mm = new MailMessage("andrei.ciornei@outlook.com", "andrei.ciornei@outlook.com", "sub", "continut");
      mm.BodyEncoding = UTF8Encoding.UTF8;
      client.EnableSsl = true;
      client.Send(mm);

      return "1";
    }

    [HttpPost]
    public string ResetPassword(string email)
    {
      string code = RandomString(20);

      string result = userService.ExistsEmail(email);
      if(result.Equals("1"))
      {
        UserDto user = new UserDto();

        user = userService.GetUserByEmail(email);
        user.Password = code;
        userService.UpdateUser(user);

        SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
        client.EnableSsl = true;
        client.Credentials = new System.Net.NetworkCredential("andrei.ciornei1@gmail.com", "PandaForce1");
        MailMessage mm = new MailMessage("andrei.ciornei1@gmail.com", email, "Reset your password", "Based on your request we generate a temporary code(" + code + ") with whom you can login in or you can reset your password here: http://localhost:57808/Home/ResetTemporaryCode/?code=" + code);
        mm.BodyEncoding = UTF8Encoding.UTF8;
        client.EnableSsl = true;
        client.Send(mm);


        return "1";
      }
      return "0";

      
    }

    private static Random random = new Random();
    public static string RandomString(int length)
    {
    const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      return new string(Enumerable.Repeat(chars, length)
        .Select(s => s[random.Next(s.Length)]).ToArray());
    }


    [HttpPost]
    public string ResetCode(string code, string newpass)
    {
      return userService.ResetCode(code, newpass);
    }

    [HttpPost]
    public string EmptyCart()
    {
      if(System.Web.HttpContext.Current.Session["prods"] != null)
      {
        System.Web.HttpContext.Current.Session["prods"] = null;

      }
      return "0";

    }

  }
}