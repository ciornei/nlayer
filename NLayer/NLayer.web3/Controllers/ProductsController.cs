﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLayer.Services.Products;
using Newtonsoft.Json;
using NLayer.Services.Dto;
using NLayer.web3.Helpers;

namespace NLayer.web3.Controllers
{
  public class ProductsController : Controller
  {
    private readonly IproductsService productService;
    public ProductsController(IproductsService productService)
    {
      this.productService = productService;
    }
    // GET: Products
    public ActionResult Index()
    {
      var wantedItems = productService.GetAll();
      ViewBag.prods = wantedItems;

      var cat = productService.GetCategories2();
      ViewBag.Categories = cat;

      return View();
    }

    [HttpPost]
    public string GetProductsByCategory(string id)
    {
      var prods = productService.GetProductsByCat2(Convert.ToInt32(id));
      return JsonConvert.SerializeObject(prods);
    }

    public ActionResult Create()
    {
      return View();
    }

    [HttpPost]
    public ActionResult Create(ProductDto product)
    {
      if(ModelState.IsValid)
      {
        productService.AddProduct(product);
        return Redirect("Index");
      }

      return View(product);
    }

    [AdminAcces]
    public ActionResult Edit(int id)
    {
      var prod = productService.Get(id);
      return View(prod);
    }
    [AdminAcces]
    [HttpPost]
    public ActionResult Edit(ProductDto product)
    {
      if (ModelState.IsValid)
      {
        productService.UpdateProduct(product);
        return Redirect("Index");
      }
      return View(product);

    }
    [AdminAcces]
    [HttpPost]
    public string DeleteProduct(string id)
    {
      productService.DeleteProduct(Convert.ToInt32(id));
      return "1";
    }
  }
}