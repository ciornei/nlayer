﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NLayer.Services.Orders;
using NLayer.Services.Users;
using NLayer.Services.Products;
using NLayer.Services.Dto;
using Newtonsoft.Json;
using NLayer.Services.Models;
using NLayer.web3.Helpers;

namespace NLayer.web3.Controllers
{
  public class OrdersController : Controller
  {
    private readonly IOrderService orderService;
    private readonly IUserService userService;
    private readonly IproductsService productService;
    // GET: User
    public OrdersController(IOrderService orderService, IproductsService productService, IUserService userService)
    {
      this.orderService = orderService;
      this.productService = productService;
      this.userService = userService;
    }
    // GET: Orders
    
    public ActionResult Index()
    {
      var users = userService.GetAll();
      var cat = productService.GetCategories();
      ViewBag.Categories = cat;
      ViewBag.Users = users;
      return View();
    }

    public ActionResult ViewCart()
    {
      var cat = productService.GetCategories();
      ViewBag.Categories = cat;
        List<ProductDto> produse = new List<ProductDto>();

      List<int> prod = new List<int>();
      if(System.Web.HttpContext.Current.Session["prods"] != null)
      {
        prod = (List<int>)System.Web.HttpContext.Current.Session["prods"];
        
        foreach(var item in prod)
        {
          produse.Add(productService.Get(item));
        }

      }
      ViewBag.produse = produse;
      
      return View();
    }

    [HttpPost]
    public string SeeOrders(string userId)
    {
      List<OrderDto> list = new List<OrderDto>();
      list = orderService.ShowOrders(Convert.ToInt32(userId));

      return JsonConvert.SerializeObject(list);
    }

    [AdminAcces]
    public ActionResult Edit(int id)
    {
      List<OrderProductsDto> produse = new List<OrderProductsDto>();
      produse = productService.GetProductsByOrder(Convert.ToInt32(id));

      var cat = productService.GetCategories();
      ViewBag.Categories = cat;

      ViewBag.produse = produse;

      return View();
    }

    [HttpPost]
    public string Checkout(List<int> ids)
    {

      return "1";
    }

    [HttpPost]
    public string GetProductsByCat(string catId, string orderID)
    {

      var prods = productService.GetProductsByCat(Convert.ToInt32(catId), Convert.ToInt32(orderID));
      return JsonConvert.SerializeObject(prods);

    }
    [AdminAcces]
    [HttpPost]
    public string RemoveOrderProduct(string orderId, string productId)
    {
      orderService.RemoveOrderProduct(Convert.ToInt32(orderId), Convert.ToInt32(productId));

      return "1";
    }

    [HttpPost]
    public string AddOrderProduct(string orderId, string productId, string quantity)
    {


      var op = new OrderProduct(Convert.ToInt32(orderId), Convert.ToInt32(productId), Convert.ToInt32(quantity));
      orderService.AddOderProduct(op);

      return "1";
    }


    public ActionResult Create()
    {

      var users = userService.GetAll();
      ViewBag.Users = users;
      var cat = productService.GetCategories();
      ViewBag.Categories = cat;
      
      if(System.Web.HttpContext.Current.Session["prods"] != null)
      {

      }

      return View();

    }

    [HttpPost]
    public string GetProductsByCategory(string catId)
    {

      var prods = productService.GetProductsByCat2(Convert.ToInt32(catId));
      return JsonConvert.SerializeObject(prods);

    }

    [HttpPost]
    public string CreateNewOrder(string userID, string prods)
    {
      var order = new OrderDto();
      order.UserID = Convert.ToInt32(userID);
      order.OrderDate = DateTime.Now;
      var obj3 = JsonConvert.DeserializeObject<List<OrderProduct>>(prods);
      order.OrderProducts = obj3;
      orderService.AddOrder(order);
      return "1";
    }
  }
}