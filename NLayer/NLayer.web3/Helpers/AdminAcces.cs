﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NLayer.web3.Helpers
{
  public class AdminAcces : AuthorizeAttribute
  {
    protected override bool AuthorizeCore(HttpContextBase httpContext)
    {
      var role = System.Web.HttpContext.Current.Session["LoggedUserRole"];
      if(role!= null)
      {
        if (role.ToString().Equals("admin"))
        {
          return true;
        }
      }
      
      return false;

    }

  }
}