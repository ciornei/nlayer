﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayer.Data.Entities;
using NLayer.Data.Infrastructure;
using NLayer.Services.Dto;
using Omu.ValueInjecter;
using NLayer.Services.Models;

namespace NLayer.Services.Orders
{
  public class OrderService : IOrderService
  {
    private readonly IRepository<Order> orderRepository;
    private readonly IRepository<Discount> discountRepository;
    private readonly IRepository<Data.Entities.OrderProduct> orderProductRepository;
    private readonly IUnitOfWork unitOfWork;

    public OrderService(IRepository<Order> orderRepository, IUnitOfWork unitOfWork)
    {
      this.orderRepository = orderRepository;
      this.unitOfWork = unitOfWork;
    }

    public void AddOderProduct(Models.OrderProduct orderprod)
    {
      var ordertoupdate = orderRepository.Query(a => a.ID.Equals(orderprod.OrderID)).FirstOrDefault();
      var ordpro = new Data.Entities.OrderProduct();
      ordpro.InjectFrom(orderprod);
      ordertoupdate.OrderProducts.Add(ordpro);
      orderRepository.Update(ordertoupdate);
      unitOfWork.Commit();
    }

    public void AddOrder(OrderDto order)
    {
      //var orderToAdd = new Order();
      //orderToAdd.InjectFrom(order);
      //orderRepository.Add(orderToAdd);
      //unitOfWork.Commit();

      //order.OrderDate = DateTime.Now;
      //var orderToAdd = new Order();
      //orderToAdd.InjectFrom(order);
      //orderRepository.Add(orderToAdd);
      //foreach (var item in order.OrderProducts)
      //{
      //  var orderProduct = new Data.Entities.OrderProduct();
      //  orderProduct.InjectFrom(item);
      //  orderProductRepository.Add(orderProduct);
      //}
      //unitOfWork.Commit();
      
      var orderToAdd = new Order();
      orderToAdd.InjectFrom(order);

      foreach (var prod in order.OrderProducts)
      {
        var ordProd = new Data.Entities.OrderProduct();
        ordProd.InjectFrom(prod);
        if(ordProd.Quantity.Equals(0) || ordProd.Quantity.Equals(null))
        {
          ordProd.Quantity = 1;
        }
        orderToAdd.OrderProducts.Add(ordProd);
        
      }

      orderRepository.Add(orderToAdd);
      unitOfWork.Commit();
    }

    public void DeleteOrder(int orderId)
    {
      //var orderToDelete = orderRepository.Query(a => a.ID.Equals(orderId)).FirstOrDefault();
      //orderRepository.Delete(orderToDelete);

      var order = orderRepository.Query(x => x.ID == orderId).FirstOrDefault();
      var orderProductsToDelete = orderProductRepository.Query(x => x.OrderID.Equals(orderId)).ToList();
      foreach (var item in orderProductsToDelete)
      {
        orderProductRepository.Delete(item);
      }
      if (order != null)
      {
        orderRepository.Delete(order);
        unitOfWork.Commit();

      }
    }

    public void RemoveOrderProduct(int orderId, int productId)
    {
      var orderToUpdate = orderRepository.Query(a => a.ID.Equals(orderId)).FirstOrDefault();
      var delete = orderToUpdate.OrderProducts.Where(a => a.ProductID.Equals(productId)).FirstOrDefault();
      orderToUpdate.OrderProducts.Remove(delete);
      orderRepository.Update(orderToUpdate);
      unitOfWork.Commit();
    }

    public List<OrderDto> ShowOrders(int userID)
    {
      var orders = orderRepository.Query(a => a.UserID.Equals(userID)).ToList();
      return GetMappedORders(orders).ToList();
    }

    public IEnumerable<OrderDto> GetMappedORders(List<Order> userList)
    {
      return userList.Select(user => (OrderDto)new OrderDto().InjectFrom(user));
    }

    public decimal GetDiscount(int orderID)
    {
      var order = orderRepository.Query(x => x.ID.Equals(orderID)).FirstOrDefault();
      var dis = new Discount();
      List<Data.Entities.OrderProduct> orderList = order.OrderProducts.ToList();
      List<Discount> discs = discountRepository.Query().OrderBy(a=> a.Value).ToList();
      foreach (var item in discs)
      {
        bool flag = true;
        int index = 0;
        for (int i = 0; ((i < orderList.Count()) && (index < item.Products.Count())); ++i)
        {
          if (item.Products.Contains(orderList[i].Product))
          {
            index += 1;
          }
        }
        if (index != item.Products.Count())
        {
          flag = false;
        }
        if (flag == true)
        {
          dis = item;
          break;
        }
      }

      return dis.Value;

    }
  }
}
