﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayer.Services.Dto;
using NLayer.Data.Entities;
using NLayer.Data.Infrastructure;
   
namespace NLayer.Services.Orders
{
  public interface IOrderService
  {
    void AddOrder(OrderDto order);
    void AddOderProduct(Models.OrderProduct orderProduct);
    void RemoveOrderProduct(int orderId, int productId);
    void DeleteOrder(int orderId);
    List<OrderDto> ShowOrders(int userID);
    decimal GetDiscount(int orderID);
    
  }
}
