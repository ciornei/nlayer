﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayer.Data.Entities;
using NLayer.Data.Infrastructure;
using NLayer.Services.Dto;
using Omu.ValueInjecter;

namespace NLayer.Services.Users
{
  public class UserService : IUserService
  {

    private readonly IRepository<User> userRepository;
    private readonly IUnitOfWork unitOfWork;


    public UserService(IRepository<User> userRepository, IUnitOfWork unitOfWork)
    {
      this.unitOfWork = unitOfWork;
      this.userRepository = userRepository;
    }

    public void AddUser(UserDto user)
    {
      var userToAdd = new User();
      userToAdd.InjectFrom(user);
      userRepository.Add(userToAdd);
      unitOfWork.Commit();
    }

    public void Delete(UserDto user)
    {
      var userToDelete = new User();
      userToDelete.InjectFrom(user);
      userRepository.Delete(userToDelete);
    }

    public List<UserDto> FindByName(string name)
    {
      var persons = userRepository.Query(a => a.FirstName.Equals(name) || a.LastName.Equals(name)).ToList();
      return GetMappedUsers(persons).ToList();
    }

    public List<UserToDisplayDto> GetAll()
    {
      var persons = userRepository.Query(a=> a.Deleted.Equals(false)).ToList();
      return GetMappedUsersDisplay(persons).ToList();
    }

    public IEnumerable<UserDto> GetMappedUsers(List<User> userList)
    {
      return userList.Select(user => (UserDto)new UserDto().InjectFrom(user));
    }

    public IEnumerable<UserToDisplayDto> GetMappedUsersDisplay(List<User> userlist)
    {
      return userlist.Select(user => (UserToDisplayDto)new UserToDisplayDto().InjectFrom(user));
    }

    public void ResetPassword(int UserId, string newPassword)
    {
      var userToUpdate = userRepository.Query(a => a.ID.Equals(UserId)).FirstOrDefault();
      userToUpdate.Password = newPassword;
    }

    public void Update(UserDto user)
    {
      var pass = userRepository.Query(a => a.ID.Equals(user.ID)).Select(a => a.Password).FirstOrDefault().ToString();
      user.Password = pass;
      var userToUpdate = new User();
      userToUpdate.InjectFrom(user);
      userRepository.Update(userToUpdate);
      unitOfWork.Commit();
    }

    public void UpdateUser(UserDto user)
    {
      var us = userRepository.Query(a => a.ID.Equals(user.ID)).SingleOrDefault();
      us.InjectFrom(user);
      userRepository.Update(us);
      unitOfWork.Commit();

    }

    public UserDto GetUserById(int userID)
    {
      var user = userRepository.Query(a => a.ID.Equals(userID)).FirstOrDefault();
      UserDto UserDto = new UserDto();
      UserDto.InjectFrom(user);
      return UserDto;
      
    }

    public void DeleteUser(int userID)
    {
      var user = userRepository.Query(a => a.ID.Equals(userID)).FirstOrDefault();
      user.Deleted = true;
      unitOfWork.Commit();

    }

    public string LoginUser(UserDto user)
    {
      //var userr = userRepository.Query(a => a.Email.Equals(user.Email) && a.Password.Equals(user.Password));
      var userr = userRepository.Query(a => a.Email==user.Email).FirstOrDefault();
      if(userr != null && userr.Password.Equals(user.Password))
      {
        return "1";
      }
      
      return "0";
    }

    public UserDto GetUserByEmail(string email)
    {
      var user = userRepository.Query(a => a.Email.Equals(email)).FirstOrDefault();
      var userToReturn = new UserDto();
      userToReturn.InjectFrom(user);
      return userToReturn;
    }

    public string ExistsEmail(string email)
    {
      var userr = userRepository.Query(a => a.Email == email).FirstOrDefault();
      if (userr != null )
      {
        return "1";
      }
      return "0";
    }

    public string ResetCode(string code, string pass)
    {

      var user = userRepository.Query(a => a.Password.Equals(code)).SingleOrDefault();
      user.Password = pass;
      userRepository.Update(user);
      unitOfWork.Commit();
      return "1";

    }

  }
}
