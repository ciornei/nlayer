﻿using System.Collections.Generic;
using NLayer.Services.Dto;

namespace NLayer.Services.Users
{
  public interface IUserService
  {
    List<UserToDisplayDto> GetAll();
    void AddUser(UserDto user);
    List<UserDto> FindByName(string name);
    void Delete(UserDto user);
    void Update(UserDto user);
    void ResetPassword(int UserId, string newPassword);
    UserDto GetUserById(int userID);
    void DeleteUser(int userID);
    string LoginUser(UserDto user);
    UserDto GetUserByEmail(string email);
    string ExistsEmail(string email);
    void UpdateUser(UserDto user);
    string ResetCode(string code, string pass);
  }
}