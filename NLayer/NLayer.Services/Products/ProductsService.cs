﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayer.Data.Entities;
using NLayer.Data.Infrastructure;
using NLayer.Services.Dto;
using Omu.ValueInjecter;
using NLayer.Services.Models;
using System;
using System.Collections.Generic;

namespace NLayer.Services.Products
{
  public class ProductsService : IproductsService
  {
    private readonly IRepository<Product> productService;

    private readonly IRepository<Data.Entities.OrderProduct> orderproductService;
    private readonly IRepository<ProductCategory> productcatrepo;

    private readonly IUnitOfWork unitOfWork;

    public ProductsService(IRepository<Product> productService, IUnitOfWork unitOfWork, IRepository<ProductCategory> productcatrepo, IRepository<Data.Entities.OrderProduct> orderproductService)
    {
      this.productService = productService;
      this.productcatrepo = productcatrepo;
      this.unitOfWork = unitOfWork;
      this.orderproductService = orderproductService;
    }

    public void AddProduct(ProductDto product)
    {
      var productToAdd = new Product();
      productToAdd.InjectFrom(product);
      productService.Add(productToAdd);
      unitOfWork.Commit();

    }

    public void DeleteProduct(int productID)
    {
      var productToDelete = productService.Query(a => a.ID.Equals(productID)).FirstOrDefault();
      productToDelete.Deleted = true;
      // productService.Delete(productToDelete);
      productService.Update(productToDelete);
      unitOfWork.Commit();
    }

    public List<ProductDto> FindByName(string productName)
    {
      var products = productService.Query(a => a.Name.Contains(productName)).ToList();
      return GetMappedProducts(products).ToList();
      
    }

    public List<ProductDto> GetAll()
    {
      var products = productService.Query(a=> a.Deleted.Equals(false)).ToList();
      return GetMappedProducts(products).ToList();
    }

    public List<ProductCategoryDto> GetCategories()
    {
      var cat = productcatrepo.Query().ToList();
      return GetMappedCategories(cat).ToList();
    }

    public List<ProductCategoryDto2> GetCategories2()
    {
      var cat = productcatrepo.Query().ToList();
      var cat2 = GetMappedCategories2(cat).ToList();
      foreach(var item in cat2)
      {
        item.NR = productService.Query(a => a.CategoryID.Equals(item.ID) && a.Deleted.Equals(false)).Count();
      }
      return cat2;
    }

    public IEnumerable<ProductDto> GetMappedProducts(List<Product> productList)
    {
      return productList.Select(user => (ProductDto)new ProductDto().InjectFrom(user));
    }
    public IEnumerable<ProductCategoryDto> GetMappedCategories(List<ProductCategory> productList)
    {
      return productList.Select(user => (ProductCategoryDto)new ProductCategoryDto().InjectFrom(user));
    }

    public IEnumerable<ProductCategoryDto2> GetMappedCategories2(List<ProductCategory> productList)
    {
      return productList.Select(user => (ProductCategoryDto2)new ProductCategoryDto2().InjectFrom(user));
    }

    public void UpdateProduct(ProductDto product)
    {
      var productToUpdate = new Product();
      productToUpdate.InjectFrom(product);
      productService.Update(productToUpdate);
      unitOfWork.Commit();
    }

    public string GetCategoryById(int id)
    {
      return productcatrepo.Query(a => a.ID.Equals(id)).Select(a=>a.Name).FirstOrDefault().ToString();
    }

    public List<OrderProductsDto> GetProductsByOrder(int orderId)
    {
      List<OrderProductsDto> list = new List<OrderProductsDto>();
      var prod = orderproductService.Query(a => a.OrderID.Equals(orderId)).ToList();
      
      foreach(var item in prod)
      {
        var it = new OrderProductsDto();
        it.InjectFrom(item);
        it.Name = productService.Query(a => a.ID.Equals(it.ProductID)).Select(a => a.Name).FirstOrDefault();
        it.Price = productService.Query(a => a.ID.Equals(it.ProductID)).Select(a => a.Price).FirstOrDefault();
        list.Add(it);
      }


      return list;

    }

    public IEnumerable<OrderProductsDto> getprods(List<Data.Entities.OrderProduct> productList)
    {
      return productList.Select(user => (OrderProductsDto)new OrderProductsDto().InjectFrom(user));
    }

    public List<ProductDto> GetProductsByCat(int categoryId, int orderId)
    {
      var prods = productService.Query(a => a.CategoryID.Equals(categoryId)).ToList();
      var it = orderproductService.Query(a => a.OrderID.Equals(orderId)).ToList();

      foreach (var item in it)
      {
        if(prods.Select(a=>a.ID).Contains(item.ProductID))
        {
          prods.Remove(prods.Where(a => a.ID.Equals(item.ProductID)).FirstOrDefault());
        }

      }


      return GetMappedProducts(prods).ToList();
    }

    public ProductDto Get(int id)
    {
      var product = new ProductDto();
      var prod = productService.Query(a => a.ID.Equals(id)).FirstOrDefault();
      product.InjectFrom(prod);
      return product;
    }

    public List<ProductDto> GetProductsByCat2(int categoryId)
    {
      var prods = productService.Query(a => a.CategoryID.Equals(categoryId) && a.Deleted.Equals(false)).ToList();
      return GetMappedProducts(prods).ToList();
    }

    public List<ProductDto> GetAll2()
    {
      var products = productService.Query(a => a.Deleted.Equals(false)).OrderByDescending(a=>a.ID).Take(12).ToList();
      return GetMappedProducts(products).ToList();
    }
  }
}
