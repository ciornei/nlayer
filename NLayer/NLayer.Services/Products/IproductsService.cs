﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayer.Services.Dto;
using NLayer.Data.Entities;
using NLayer.Data.Infrastructure;

namespace NLayer.Services.Products
{
  public interface IproductsService
  {
    void AddProduct(ProductDto product);
    void DeleteProduct(int productID);
    void UpdateProduct(ProductDto product);
    List<ProductDto> GetAll();
    List<ProductDto> GetAll2();
    List<ProductDto> FindByName(string productName);
    List<ProductCategoryDto> GetCategories();
    string GetCategoryById(int id);
    List<OrderProductsDto> GetProductsByOrder(int orderId);
    List<ProductDto> GetProductsByCat(int categoryId, int orderId);
    List<ProductDto> GetProductsByCat2(int categoryId);
    ProductDto Get(int id);
    List<ProductCategoryDto2> GetCategories2();
    

  }
}
