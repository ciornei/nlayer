﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLayer.Services.Models
{
  [Serializable]
  public class Root
  {
    public List<OrderProduct> RootObj { get; set; }

  }
}
