﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLayer.Services.Models
{
  [Serializable]
  public class OrderProduct
  {
    
    public int OrderID { get; set; }
    public int ProductID { get; set; }
    public int Quantity { get; set; }

    public OrderProduct(int orderid, int productid, int quantity)
    {
      this.OrderID = orderid;
      this.ProductID = productid;
      this.Quantity = quantity;
    }
  }
}
