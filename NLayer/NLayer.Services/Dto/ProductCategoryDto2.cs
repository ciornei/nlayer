﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLayer.Services.Dto
{
  public class ProductCategoryDto2
  {
    public int ID { get; set; }
    public string Name { get; set; }
    public int NR { get; set; }
  }
}
