﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace NLayer.Services.Dto
{
  public class ProductDto
  {
    [Required]
    public int ID { get; set; }

    [Display(Name="Numele produsului")]
    [Required(ErrorMessage = "Tre nume ") ]
    public string Name { get; set; }

    [Required(ErrorMessage = "Tre pretz ")]
    [Range(minimum:1.0, maximum: 99999.9, ErrorMessage = "Tre in range")]
    [DataType(DataType.Currency)]
    public Nullable<decimal> Price { get; set; }

    [Display(Name = "Categorie")]
    [Required(ErrorMessage = "Tre categorie")]
    public int CategoryID { get; set; }
    
    public string CategoryName { get; set; }

  }
}
