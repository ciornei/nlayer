﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLayer.Services.Dto
{
  public class OrderProductsDto
  {
    public int OrderID { get; set; }
    public int ProductID { get; set; }
    

    public int Quantity { get; set; }


    public string Name { get; set; }
    public Nullable<decimal> Price { get; set; } 
  }
}
