﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLayer.Services.Models;
using System.ComponentModel.DataAnnotations;

namespace NLayer.Services.Dto
{
  public class OrderDto
  {
    [Key]
    public int ID { get; set; }
    
    public int UserID { get; set; }
    
    public Nullable<int> ShipmentAddressID { get; set; }

    [Required]
    public Nullable< DateTime> OrderDate { get; set; }

    
    public List<OrderProduct> OrderProducts { get; set; }
    
  
  }
}
