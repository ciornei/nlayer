﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLayer.Services.Dto
{
  public class UserDto
  {

    public int ID { get; set; }

    [Required(ErrorMessage = "Tre' mail")]
    [DataType(DataType.EmailAddress)]
    [RegularExpression("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$", ErrorMessage = "Must be a valid email")]
    public string Email { get; set; }
    
    [Required(ErrorMessage = "Tre' nume")]
    public string FirstName { get; set; }

    [Required(ErrorMessage = "Tre' nume")]
    public string LastName { get; set; }

    //[RegularExpression(@"^((?=.*[a-z])(?=.*[A-Z])(?=.*\d)).+$")]
    //[RegularExpression("^(?=(?:.*[A-Z]){3})(?=(?:.*[^a-zA-Z]){4})")]
    //[RegularExpression(@"^.(?=.{6,})(?=.[a-z])(?=.[A-Z])(?=.[\d\W]).*$")]
    [RegularExpression(@"^(((?=.*[a-z])(?=.*[A-Z])(?=.*[\d]))|((?=.*[a-z])(?=.*[A-Z])(?=.*[\W]))|((?=.*[a-z])(?=.*[\d])(?=.*[\W]))|((?=.*[A-Z])(?=.*[\d])(?=.*[\W]))).{8,30}$", ErrorMessage = "Password must be in 8-30 range and  contain 3 out of the 4 of these: \n• lowercase letter \n• uppercase letter \n• number \n• special character ! @ $ % ^ &*( ) + ? ")]
    //[RegularExpression("^.(?=.{6,})(?=.[a-z])(?=.[A-Z]).$")]
    //[RegularExpression(@"^(?=[^\d_].*?\d)\w(\w|[!@#$%]){7,20}")]
    [DataType(DataType.Password)]
    [Required(ErrorMessage = "Tre' parola")]
    public string Password { get; set; }

    [Required(ErrorMessage = "Tre' rol")]
    public string Role { get; set; }


    public bool Deleted { get; set; }
    
  }
}
