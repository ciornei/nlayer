﻿using NLayer.Data.Infrastructure;
using NLayer.Services.Users;
using NLayer.Services.Orders;
using NLayer.Services.Products;
using SimpleInjector;

namespace NLayer.Services.Infrastructure
{
  public static class SimpleInjectorInitializer
  {
    public static void InitializeContainer(Container container, Lifestyle lifeStyle)
    {
      //database
      container.Register<IDatabaseFactory, DatabaseFactory>(lifeStyle);
      container.Register<IUnitOfWork, UnitOfWork>(lifeStyle);

      //register generic repository
      container.Register(typeof(IRepository<>), typeof(Repository<>), lifeStyle);

      //services
      container.Register<IUserService, UserService>(lifeStyle);
      container.Register<IOrderService, OrderService>(lifeStyle);
      container.Register<IproductsService, ProductsService>(lifeStyle);

    }
  }
}
