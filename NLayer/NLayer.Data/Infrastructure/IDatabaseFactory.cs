﻿using System;
using System.Data.Entity;

namespace NLayer.Data.Infrastructure
{
  public interface IDatabaseFactory : IDisposable
  {
    DbContext Get();
  }
}
