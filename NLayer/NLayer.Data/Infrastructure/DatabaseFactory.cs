﻿using System.Data.Entity;
using NLayer.Data.Entities;

namespace NLayer.Data.Infrastructure
{
  public class DatabaseFactory : Disposable, IDatabaseFactory
  {
    private DbContext dataContext;

    public DbContext Get()
    {
      if (dataContext != null) return dataContext;
      dataContext = new NLayerSampleEntities();
      dataContext.Configuration.UseDatabaseNullSemantics = true;
      return dataContext;
    }

    protected override void DisposeCore()
    {
      if (dataContext != null)
        dataContext.Dispose();
    }
  }
}
