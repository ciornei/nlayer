﻿
namespace NLayer.Data.Infrastructure
{
  public interface IUnitOfWork
  {

    void Commit();
  
  }
}
